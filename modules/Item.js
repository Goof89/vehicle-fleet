const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// Create Schema
const ItemSchema = new Schema({
    id: {
        type: String,
        required : true
    },
    name: {
        type: String,
        required : true
    },
    carType: {
        type: String,
        required : true
     },
    timeCreated: {
        type: Date,
        default: Date.now
    }
});

module.exports = Item = mongoose.model('item', ItemSchema);