import React, { Component } from 'react';
import { Provider } from 'react-redux';
import store from './store';
import {Container} from 'reactstrap';
import 'bootstrap/dist/css/bootstrap.min.css';


import AppNavbar from './components/AppNavbar';
import VehicleList from './components/ItemList';


import './App.css';

class App extends Component {
  render() {
    return (
      <Provider store ={store}>
        <div className="App">
            <AppNavbar />
            <Container>
              <VehicleList />
            </Container>
        </div>
      </Provider>
    );
  }
}

export default App;
