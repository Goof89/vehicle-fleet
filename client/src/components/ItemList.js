import React, {Component} from 'react';
//import {Container, ListGroup, ListGroupItem, Button} from 'reactstrap';
//import {CSSTransition, TransitionGroup} from 'react-transition-group';
import uuid from "uuid";
import {
  Button,
  Modal,
  ModalHeader,
  ModalBody,
  Form,
  FormGroup,
  Label,
  Input,
  ModalFooter
} from 'reactstrap';

import { connect } from 'react-redux';
import { getItems, addItem, updateItem, deleteItem } from '../actions/ItemActions';
import PropTypes from 'prop-types';
import BootstrapTable from 'react-bootstrap-table-next';
import  cellEditFactory, { Type } from 'react-bootstrap-table2-editor';

class ItemList extends Component {

    state = {
      modal: false,
      name: '',
      carType: 'SUV',
      selected: [],
      idToDelete: null
    };

    componentDidMount() {
      this.props.getItems();
    };

    onDeleteClick = () => {
      this.props.deleteItem(this.state.idToDelete);
      this.setState({ selected: [], idToDelete: null });
    };

    afterSaveCell = (oldValue, newValue, row, column) => {
      const payload = {
        update: {[column.dataField]: newValue},
        id: row._id
      };
      this.props.updateItem(payload);
    };

    toggle = () => {
      this.setState({
        modal: !this.state.modal,
        name: ""
      });
    };

    onChange = e => {
      this.setState({[e.target.name]: e.target.value});
    };

    onSubmit = e => {
      e.preventDefault();

      const newItem = {
        id: uuid(),
        name: this.state.name,
        carType: this.state.carType
      };

      this.props.addItem(newItem);

      // Close modal
      this.toggle();
    };

    handleOnSelect = (raw) => {
      this.setState({ selected: [raw.id], idToDelete: raw._id });
    };

  render() {
      const { items } = this.props.item;

      const columns = [{
        dataField: 'id',
        text: 'Item ID'
      }, {
        dataField: 'name',
        text: 'Vehicle Name'
      }, {
        dataField: 'carType',
        text: 'Car Type',
        editor: {
          type: Type.SELECT,
          options: [{
            value: 'SUV',
            label: 'SUV'
          }, {
            value: 'Truck',
            label: 'Truck'
          }, {
            value: 'Hybrid',
            label: 'Hybrid'
          }]
        }
      }, {
        dataField: 'timeCreated',
        text: 'Time Created',
        editable: false
      }];

      const selectRow = {
        mode: 'radio',
        clickToEdit: true,
        selected: this.state.selected,
        bgColor: 'rgb(238, 193, 213)',
        onSelect: this.handleOnSelect.bind(this)
      };

      return (
        <div>
          <Button color="success"
                  onClick={this.toggle}
          >Add Vehicle +
          </Button>
          <Button color="danger"
                  onClick={this.onDeleteClick}
                  disabled={this.state.idToDelete === null}
          >Delete Vehicle -
          </Button>
          <Modal isOpen={this.state.modal} toggle={this.toggle} >
            <ModalHeader toggle={this.toggle}>Add New Vehicle</ModalHeader>
            <ModalBody>
              <Form onSubmit={this.onSubmit}>
                <FormGroup>
                  <Label for="item">Vehicle name</Label>
                  <Input type="text" name="name" id="item" placeholder="Vehicle Name" onChange={this.onChange} value={this.state.name}/>
                </FormGroup>
                <FormGroup>
                  <Label for="exampleSelect">Car Type</Label>
                  <Input type="select" name="carType" id="exampleSelect" onChange={this.onChange} value={this.state.carType}>
                    <option>SUV</option>
                    <option>Truck</option>
                    <option>Hybrid</option>
                  </Input>
                </FormGroup>
              </Form>
            </ModalBody>
            <ModalFooter>
              <Button color="primary" onClick={this.onSubmit} disabled={this.state.name === ''}>Add</Button>
              <Button color="secondary" onClick={this.toggle}>Cancel</Button>
            </ModalFooter>
          </Modal>
          {items.length === 0 ? "" :
          <BootstrapTable
            keyField='id'
            data={ items }
            columns={ columns }
            selectRow={ selectRow }
            cellEdit={ cellEditFactory({ mode: 'click', afterSaveCell: this.afterSaveCell, blurToSave: true}) }
            noDataIndication="Table is Empty"
          /> }
        </div>
      )
    }
}

ItemList.propTypes = {
  getItems: PropTypes.func.isRequired,
  item: PropTypes.object.isRequired
};

const mapStateToProps = (state) => ({
  item: state.item
});

export default connect(
  mapStateToProps,
  {getItems, addItem, updateItem, deleteItem}
)(ItemList);